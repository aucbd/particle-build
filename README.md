# particle-build

Build tool written in Python to simplify compilation and deployment of firmware to Particle devices from a git repository.

A valid Particle account is needed for the tool to work. A 1 hour token is requested from the API and used for all operations. The account requirement can be skipped with the variables option (see [Arguments](#arguments)).

The credentials for the account are asked each time the tool is run, unless saved using the save credentials option. The file containing the credentials is not tracked by this repository.

When the tool starts, it computes the version of the code in the format `YYYY-MM-DD_[hash]` with the date and short hash (7 digits) of the last commit (e.g. `2020-01-09_9308e06`).

The tool takes all sources files from `src/` and copies them under `src_copy/` inside the build tool folder. This folder is not tracked by the repository.

The tool then checks the files hashes against the ones recorded in the previous call. If the hashes are unchanged, then the code will compile only if the binary is not present or if the test and/or force options are used. If the hashes are changed (or if the tool is running for the first time) then the new hashes are saved into a `build_hash.json` file located inside the build tool folder. The hashes file is not tracked by the repository.

Before the compilation, a `variables.h` file is created in the `src/` (copy) folder containing the settings passed in the build configuration (see [Build Targets](#build-targets)), the branch of the repository and the code version. A final `SETTINGS` macro is added as a semicolon-joined string of all the settings added to the file.

The final `variables.h` file will have the following format:

```C++
// THIS IS A GENERATED FILE
// DO NOT EDIT
#pragma once

#define BRANCH "master"
#define VERSION "2020-01-09_9308e06"
#define SETTING1
#define SETTING2 123
#define SETTING3 "Hello world!"
#define SETTINGS "SETTING1; SETTING2 123; SETTING3 \"Hello world!\""
```

The compiled binaries are saved inside a `bin/` folder placed in the root of the project. The binaries are saved in the format `[platform]_[deviceos]_[version]_[settings hash].bin`, where the settings hash is the short hash (7 digits) computed from the product settings in the configuration file.

The tool can only compile and save the binary of a clean repository (i.e. all changes have been committed). The test flag can be used to test the compilation of a dirty repository, and the force flag can be used to force the compilation and flashing of a dirty repository, but `_dirty` will be appended to the version if the repository is dirty.

If the compilation or flashing fails, the output from the Particle compiler will be displayed.

After each compilation there is a non-skippable wait of 5 seconds, 20 seconds after each flashing. This is to respect Particle's API limits.

## Arguments

* `-t, --test` Compile products, but do not save the binaries. Works with dirty repositories.
* `--force` Force processing independent of the state of the repository.
* `-v, --variables` Only create variables.h without compiling/flashing. Needs a single product/device target. The generated file is moved to the original `src/` folder.
* `-s, --save-credentials` Save credentials to build_credentials.json.
* `-f, --flash` Flash compiled binaries to the devices.
* `-p, --products [product1] [product2] [...]` Restrict processing to the products listed after the flag.
* `-d, --devices [device1] [device2] [...]` Restrict processing to the devices listed after the flag.
* `-g, --groups [group1] [group2] [...]` Restrict processing to the groups listed after the flag.
* `-k, --keep` Keep the copy of the source files.

If no arguments are given, the tool defaults to compiling all products and saving the binaries.

## Build Targets

The build tool uses a JSON-formatted file to configure build targets. The file must be named `build_config.json` and be placed in the root folder of the Particle project.

The file contains a root object with a `products` list field. Inside the list go product objects; these must contain the following fields:

* `"name": string` The name given to the product. It does not have to be unique and is only used in the output for clarity.
* `"platform": string` The target platform: argon, boron, etc...
* `"deviceos": string` The deviceOS version to use for compilation.
* `"settings": list[string]` A list of macros to be added to the `variables.h` file.
* `"devices": list[string]` A list of devices, both serial numbers and device names can be used.
* `"test": bool` Optional, defaults to `false`. If set to `true` then the product is used only if called via the `--products` argument or if using the `--test` flag.

An optional `groups` object field can be added to the root object containing groups of product in the form `key: list[string]`. These groups can be selected using the `--groups` argument and will be added to any prodcuts already added via the `--prodcuts` argument.

The final configuration file should have this format:

```json
{
  "groups": {
    "group_1": [
      "product1",
      "product2"
    ]
  },
  "products": [
    {
      "name": "debug",
      "platform": "argon",
      "deviceos": "1.5.2",
      "settings": ["WIFI", "DEBUG"],
      "devices": [
        "DEVICE1_NAME",
        "DEVICE2_SERIAL"
      ],
      "test": true
    },
    {
      "name": "product1",
      "platform": "argon",
      "deviceos": "1.4.4",
      "settings": ["WIFI", "INT_MACRO 123", "STRING_MACRO \"Hello world!\""],
      "devices": [
        "DEVICE1_NAME",
        "DEVICE2_SERIAL"
      ]
    },
    {
      "name": "product2",
      "platform": "boron",
      "deviceos": "1.4.0",
      "settings": ["CELLULAR"],
      "devices": [
        "DEVICE3_NAME"
      ]
    }
  ]
}
```

## Requirements

* Python 3.8 or subsequent versions.

* Python modules requirements are contained in the `requirements.txt` file and can be installed with `pip3 install -r requirements.txt`.

* All source code files must be within the `src/` folder in the project root.

* An internet connection is necessary for compilation and flashing, but not to build the `variables.h` file

## Install the Tool

If you are using a git repository, clone this repository as a submodule in the root folder of the Particle project.
```bash
git submodule add git@bitbucket.org:aucbd/particle-build.git
```

If you prefer not to use a submodule, download this repository directly in the root folder of the Particle project.

A `build_config.json` file must be created in the root folder of the Particle project containing the build targets (see [Build Targets](#build-targets)).

The final project tree should be in this form (the build folder can be named differently):

```
Project/
├── bin/
├── particle-build/
│   └── build.py
├── src/
└── build-config.json
```

## Usage

With the build tool and configuration file in place, the tool can be used directly from the command line.

```bash
python3.8 particle-build/build.py [arguments]
```

If no `build_credentials.json` file is found, the tool will ask for a username and password to connect to Particle's cloud.

If authentication succeeds, the tool proceeds to compile/flash the project following the build targets and settings provided.

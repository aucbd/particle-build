from argparse import ArgumentParser
from argparse import Namespace
from datetime import datetime
from distutils.dir_util import copy_tree
from hashlib import sha1
from json import dump
from json import dumps as json_dumps
from json import load
from os import makedirs
from os import path
from pathlib import Path
from re import match
from shutil import rmtree
from sys import argv
from time import sleep
from typing import TypedDict

from git import Repo
from requests import Response
from requests import Session

host: str = "https://api.particle.io"
platform_ids: dict[str, str] = {
    "argon": "12",
    "boron": "13",
    "xenon": "14",
}


class Configuration(TypedDict):
    name: str
    platform: str
    deviceos: str
    product: str | None
    settings: list[str]
    devices: list[str]


def timer(delay: int):
    for i in range(delay):
        print(f"\rTimeout: {delay - i}  ", end="", flush=True)
        sleep(1)
    print(f"\r{' ' * 12}\r", end="", flush=True)


def list_files(folder: Path, ignore: list[str]) -> list[Path]:
    return [
        f2
        for f1 in folder.iterdir()
        for f2 in (list_files(f1, ignore) if f1.is_dir() else [f1])
        if not any(match(f"^{i}$", str(f2.relative_to(folder))) for i in ignore)
    ]


def file_hash(file: Path) -> str:
    block_size = 65536
    hash_handler = sha1()

    with file.open("rb") as fb:
        buf = fb.read(block_size)
        while len(buf) > 0:
            hash_handler.update(buf)
            buf = fb.read(block_size)

    return hash_handler.hexdigest()


def files_hashes(files: list[Path]) -> dict[str, str]:
    return {f.name: file_hash(f) for f in files}


def authenticate(usr: str, pwd: str) -> tuple[Session, str, str]:
    data: dict = {
        "grant_type": "password",
        "username": usr,
        "password": pwd,
        "expires_in": 3600,
    }

    session: Session = Session()
    res: Response = session.post(host + "/oauth/token", auth=("particle", "particle"), data=data)
    res.raise_for_status()

    session.headers["Authorization"] = f"Bearer {res.json()['access_token']}"

    return session, res.json()['access_token'], datetime.now().timestamp() + 3600 - res.elapsed.seconds


def build_variables(conf: list[str], branch: str, version: str, settings: str, dest: Path):
    with open(dest / "variables.h", "w") as f:
        f.write("// THIS IS A GENERATED FILE\n")
        f.write("// DO NOT EDIT\n")
        f.write("#pragma once\n\n")
        f.write(f'#define BRANCH "{branch}"\n')
        f.write(f'#define VERSION "{version}"\n')
        f.write(f'#define SETTINGS "{settings}"\n')
        for c in conf:
            f.write(f"#define {' '.join(c.strip().split(' ', 1))}\n")


def build_firmware(session: Session, files: list[Path], platform: str, deviceos: str) -> tuple[bool, str]:
    assert platform in platform_ids, f"Unknown platform {platform!r}"

    files_data: dict[str, tuple] = {
        "product_id": (None, platform_ids[platform]),
        "build_target_version": (None, deviceos),
    }

    for i, file in enumerate(files):
        files_data["file" + (str(i + 1) if i else "")] = (str(file), file.open("rb"))

    res: Response = session.post(host + "/v1/binaries", files=files_data)
    if not res.json():
        res.raise_for_status()
    elif res.json()["ok"]:
        return True, res.json()["binary_url"]
    else:
        return False, res.json()["output"] + "\n".join(res.json()["errors"])


def flash_firmware(session: Session, firmware_path: Path, device: str, product: str = None) -> tuple[bool, str]:
    files_data: dict = {
        "file_type": (None, "binary"),
        "file": (str(firmware_path), firmware_path.open("rb")),
    }

    url: str = host
    if product:
        url += f"/v1/products/{product}/devices/{device}"
    else:
        url += f"/v1/devices/{device}"

    res: Response = session.put(f"{url}/ping")

    if "error" in res.json():
        return (
            False,
            res.json()["error"] + (("\n" + res.json()["info"]) if "info" in res.json() else ""),
        )
    elif not res.json()["online"]:
        return False, "not online"

    res = session.put(url, files=files_data)

    return (
        (status := res.status_code == 200),
        res.json()["status"] if status else json_dumps(res.json(), indent=2),
    )


def download(session: Session, url: str, dest: Path):
    makedirs(path.dirname(dest), exist_ok=True)

    stream: Response = session.get(host + url, stream=True)
    stream.raise_for_status()

    with dest.open("wb") as file:
        for chunk in stream.iter_content(chunk_size=1024):
            file.write(chunk)


def build_configuration(conf: Configuration, branch: str, version: str, root: Path, src: Path,
                        force: bool, variables_only: bool, flash: bool, session: Session):
    print(f"# Compile: {conf['name']}")

    platform: str = conf["platform"]
    deviceos: str = conf["deviceos"]
    product: str = conf["product"]
    settings: list[str] = sorted(conf["settings"])
    settings_hash = sha1(";".join(settings).encode()).hexdigest()[-7:]
    devices: list[str] = conf["devices"]

    (root / "bin").mkdir(exist_ok=True)
    binary: Path = root / "bin" / f"{platform}_{deviceos}_{version}_{settings_hash}.bin"

    print(f"Platform: {platform}")
    print(f"DeviceOS: {deviceos}")
    print(f"Product : {product}")
    print(f"Version : {version}")
    print(f"S. Hash : {settings_hash}")
    print(f"Settings: {settings}")
    print(f"Devices : {devices}")

    build_variables(settings, branch, version, settings_hash, src)

    if variables_only:
        print("\nvariables.h created\n")
        return

    if binary.is_file() and not force:
        print(f"\nFirmware exists: {binary.relative_to(root)}\n")
    else:
        print(f"Source  :")
        ignore: list[str] = bi.read_text().split("\n") if (bi := root / "build_ignore").is_file() else []
        files: list[Path] = sorted(list_files(src, ignore), key=lambda f: f.name)
        if (pf := root / "project.properties").is_file():
            files.append(pf)

        for file in files:
            print(f"  {file.relative_to(src) if file.is_relative_to(src) else file.relative_to(root)}")
        print()

        suc, out = build_firmware(session, files, platform, deviceos)
        if suc:
            download(session, out, binary)
            print(f"Saved firmware to: {binary.relative_to(root)}\n")
        else:
            print(out)

        timer(5)

    if not binary.is_file():
        return
    elif not flash:
        return

    for device in conf["devices"]:
        print(f"## Flashing to: {device}")
        suc, out = flash_firmware(session, binary, device, product or None)
        if suc:
            print("  " + out)
        else:
            print("  Error:")
            for err in out.split("\n"):
                print("  " + err)
        timer(20)
    print()


def main(*args: str):
    # Paths
    build_dir: Path = Path(__file__).parent
    copy_dir: Path = build_dir / "src_copy"
    root_dir: Path = Path(__file__).parent.parent
    src_dir: Path = root_dir / "src"
    conf_file: Path = root_dir / "build_config.json"

    assert conf_file.is_file()
    assert src_dir.is_dir()

    # Arguments
    args_parser: ArgumentParser = ArgumentParser()
    args_parser.add_argument("-t, --test", dest="test", default=False, action="store_true", required=False,
                             help="Test compilation without downloading binary.")
    args_parser.add_argument("--force", dest="force", default=False, action="store_true", required=False,
                             help="Force compilation regardless of repository status.")
    args_parser.add_argument("-v, --variables", dest="variables", default=False, action="store_true", required=False,
                             help="Only create variables.h without compiling/flashing")
    args_parser.add_argument("-s, --save-credentials", dest="save_credentials", default=False, action="store_true",
                             required=False, help="Save credentials to build_credentials.json.")
    args_parser.add_argument("-g, -groups", dest="groups", nargs="+", default=[], required=False,
                             help="Settings groups to process.")
    args_parser.add_argument("-p, -products", dest="products", nargs="+", default=[], required=False,
                             help="Products to process.")
    args_parser.add_argument("-d, --devices", dest="devices", nargs="+", default=[], required=False,
                             help="Devices to process.")
    args_parser.add_argument("-g, --groups", dest="groups", nargs="+", default=[], required=False,
                             help="Groups of products to process.")
    args_parser.add_argument("-f, --flash", dest="flash", default=False, action="store_true", required=False,
                             help="Flash compiled binaries to devices.")
    args: Namespace = args_parser.parse_args(args)

    if args.variables and not (len(args.groups) == 1 or len(args.products) == 1 or len(args.devices) == 1):
        args_parser.error("To build variables, a target groups/product/device is needed: "
                          "-g, --groups, -products, --products, -d, --devices")

    # Repository
    repository: Repo = Repo(".")
    branch: str = repository.active_branch.name
    version: str = (head := repository.commit("HEAD")).committed_datetime.strftime("%Y-%m-%d") + "_" + head.hexsha[:7]

    assert not repository.is_dirty() or args.test or args.force

    if repository.is_dirty():
        version += "_dirty"

    # Hashes
    if not args.test and not args.force:
        hashes_file: Path = build_dir / "build_hash.json"
        ignore: list[str] = bi.read_text().split("\n") if (bi := root_dir / "build_ignore").is_file() else []
        prev_hashes: dict[str, str] = load(hashes_file.open()) if hashes_file.is_file() else {}
        curr_hashes: dict[str, str] = files_hashes(list_files(src_dir, ignore))
        if curr_hashes != {k: v for k, v in prev_hashes.items() if k != "version"}:
            dump(curr_hashes | {"version": version}, hashes_file.open("w"))
        else:
            version = prev_hashes.get("version", version)

    # Configuration
    configurations: list[Configuration] = load(conf_file.open())

    if args.groups:
        configurations = [c for c in configurations if c["name"] in args.groups]

    if args.products:
        configurations = [c for c in configurations if c["product"] in args.products]

    if args.devices:
        # noinspection PyTypeChecker
        configurations = [c | {"devices": [d for d in c["devices"] if d in args.devices]} for c in configurations]
        configurations = [c for c in configurations if c["devices"]]

    # Authenticate
    session: Session = Session()

    if not args.variables:
        print("# Login")
        credentials: dict[str, str] = {}

        if (f := (build_dir / "build_credentials.json")).is_file():
            credentials = load(f.open())

        if "token" in credentials and credentials.get("token_expiration", 0) > datetime.now().timestamp():
            session.headers["Authorization"] = f"Bearer {credentials['token']}"
            print(f"Token: {'*' * len(credentials['token'])}")
        elif "username" in credentials and "password" in credentials:
            user_split: list[str] = credentials["username"].split("@")
            print(f"Username: {'*' * len(user_split[0])}@{user_split[1]}")
            print(f"Password: {'*' * len(credentials['username'])}")
            session, token, token_expiration = authenticate(credentials["username"], credentials["password"])
            credentials |= {"token": token, "token_expiration": token_expiration}
        else:
            credentials["username"] = input("Username: ")
            credentials["password"] = input("Password: ")
            session, token, token_expiration = authenticate(credentials["username"], credentials["password"])
            credentials |= {"token": token, "token_expiration": token_expiration}

        if args.save_credentials:
            dump(credentials, f.open("w"))

        print()

    # Copy src
    if not args.variables:
        if copy_dir.is_dir():
            rmtree(copy_dir)
        copy_tree(str(src_dir), str(copy_dir))

    for conf in configurations:
        build_configuration(conf, branch, version, root_dir,
                            src_dir if args.variables else copy_dir,
                            args.force, args.variables, args.flash, session)


if __name__ == '__main__':
    try:
        main(*argv[1:])
    except KeyboardInterrupt as _err:
        print(repr(_err))
